
# aizuda-components

![logo](https://portrait.gitee.com/uploads/avatars/namespace/2879/8637007_aizuda_1636162864.png!avatar100)

- 爱组搭 ~ 低代码组件化开发平台之组件库


- 愿景：每个人都是架构师

![微信群](https://images.gitee.com/uploads/images/2021/1119/095213_7ac4e4e7_12260.png "aizuda.png")

[爱组搭 ~ 组件源码示例演示](https://gitee.com/aizuda/aizuda-components-examples)

# 模块介绍

- aizuda-security

  安全模块，主要内容 api 请求解密，响应加密，单点登录 等。
```xml
<dependency>
  <groupId>com.aizuda</groupId>
  <artifactId>aizuda-security</artifactId>
  <version>0.0.1</version>
</dependency>
```

- aizuda-limiter

  限流模块，主要内容 api 限流，短信，邮件 发送限流、控制恶意利用验证码功能 等。

```xml
<dependency>
  <groupId>com.aizuda</groupId>
  <artifactId>aizuda-limiter</artifactId>
  <version>1.0.0</version>
</dependency>
```


